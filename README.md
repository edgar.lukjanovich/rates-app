# Currency rates React App 

Create application using data from https://api.exchangeratesapi.io/latest

### Requirements:
- Initially there should be a button. Data is fetched when the button is clicked
- "Loading" message/UI component should be shown while data is fetched
- Error modal should be shown when data fetch fails
- Application parts should be tested
- React framework
- Application state management using Redux or GraphQL client-side framework
- Pagination and styling is not necessary, table of exhange rates will be good enough
- Code should be pushed to remote GIT repository for review

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.<br>

### `npm run build`

Builds the app for production to the `build` folder.<br>
