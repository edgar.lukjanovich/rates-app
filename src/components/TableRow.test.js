import React from 'react';
import { TableRow } from './TableRow';
import { initialState } from '../store/reducers'
import renderer from 'react-test-renderer';

describe(TableRow, () => {
    const ratesArr = Object.entries(initialState.rates);

    test('Link changes the class when hovered', () => {
        const component = renderer.create(
            ratesArr.map((item, key) => (<TableRow item={item} key={key} />))
        );
        let tree = component.toJSON();
        expect(tree).toMatchSnapshot();
    });
});
