import React from 'react';

export const TableRow = ({item}) => (
    <tr id={item.key}>
        <td className="px-3 text-monospace">{item[0]}</td>
        <td align="right">{item[1].toFixed(4)}</td>
    </tr>
);
