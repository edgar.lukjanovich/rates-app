import { connect } from 'react-redux';
import React, { Component } from 'react';
import { ToastContainer } from 'react-toastify';
import { TableRow } from './components/TableRow';
import 'react-toastify/dist/ReactToastify.css';
import { callApi } from './store/actions';
import './App.css';

const mapStateToProps = (state) => {
    return {
        base: state.base,
        date: state.date,
        rates: state.rates,
        loading: state.loading,
        isLoaded: state.isLoaded,
    };
};

class App extends Component {
  render() {
      const { dispatch, date, rates, base, loading } = this.props;
      const ratesArr = Object.entries(rates);
      return <div className="App">
          <ToastContainer />
          <h1 className="pt-4 pb-2">Currency rates <span className="badge badge-info">{base}</span></h1>
          {date && <h3 className="pb-4">for {date}</h3>}
          <button onClick={() => dispatch(callApi())} type="button" className="btn btn-primary">
              {loading && <span className="spinner-border spinner-border-sm mr-3" role="status" aria-hidden="true"/>}
              Get rates
          </button>
          <table className="my-4">
              <tbody>
                  {ratesArr.map((item, key) => (<TableRow item={item} key={key} />))}
              </tbody>
          </table>

      </div>;
  }
}

export default connect(mapStateToProps)(App);
