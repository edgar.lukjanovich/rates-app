import { toast } from 'react-toastify';
import Axios from 'axios/index';

export const ACTION_RATES_START = 'ACTION_RATES_START';
export const ACTION_RATES_SUCCESS = 'ACTION_RATES_SUCCESS';
export const ACTION_RATES_ERROR = 'ACTION_RATES_ERROR';

export function callApi () {
    const url = 'https://api.exchangeratesapi.io/latest';
    return (dispatch) => {
        Axios.get(url)
            .then((res) => {
                dispatch({
                    type: ACTION_RATES_SUCCESS,
                    payload: res.data,
                });
            })
            .catch((err) => {
                dispatch({
                    type: ACTION_RATES_ERROR,
                    payload: err,
                });
                toast.error('Looks, like something went wrong!\n' + err);
            });
        return dispatch({
            type: ACTION_RATES_START,
        });
    };
}
