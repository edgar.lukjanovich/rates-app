import { ACTION_RATES_ERROR, ACTION_RATES_START, ACTION_RATES_SUCCESS } from './actions';

export const initialState = {
    base: '',
    rates: {
        '- - -': 0,
    },
    date: null,
    error: false,
    loading: false,
    isLoaded: false
};

export const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_RATES_START:
            return {
                ...state,
                loading: true,
                error: false,
                isLoaded: false,
            };
        case ACTION_RATES_SUCCESS:
            return {
                ...action.payload,
                loading: false,
                error: false,
                isLoaded: true,
            };
        case ACTION_RATES_ERROR:
            return {
                ...state,
                loading: false,
                error: true,
                isLoaded: true,
                errorMsg: action.payload,
            };
        default:
            return state;
    }
};
